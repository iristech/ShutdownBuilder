﻿namespace WindowsFormsApplication1
{
    partial class MainWindow
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.label1 = new System.Windows.Forms.Label();
            this.launchShutdown = new System.Windows.Forms.Button();
            this.hourNumber = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.minuteNumber = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.rebootCb = new System.Windows.Forms.CheckBox();
            this.cancelShutdown = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.dayNumber = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.hourNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minuteNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dayNumber)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Arrêt planifié dans :";
            // 
            // launchShutdown
            // 
            this.launchShutdown.Location = new System.Drawing.Point(149, 86);
            this.launchShutdown.Name = "launchShutdown";
            this.launchShutdown.Size = new System.Drawing.Size(120, 23);
            this.launchShutdown.TabIndex = 2;
            this.launchShutdown.Text = "Lancer l\'arrêt planifié";
            this.launchShutdown.UseVisualStyleBackColor = true;
            this.launchShutdown.Click += new System.EventHandler(this.launch_Click);
            // 
            // hourNumber
            // 
            this.hourNumber.Location = new System.Drawing.Point(203, 21);
            this.hourNumber.Name = "hourNumber";
            this.hourNumber.Size = new System.Drawing.Size(44, 20);
            this.hourNumber.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(253, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Heures";
            // 
            // minuteNumber
            // 
            this.minuteNumber.Location = new System.Drawing.Point(300, 21);
            this.minuteNumber.Name = "minuteNumber";
            this.minuteNumber.Size = new System.Drawing.Size(42, 20);
            this.minuteNumber.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(348, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Minutes";
            // 
            // rebootCb
            // 
            this.rebootCb.AutoSize = true;
            this.rebootCb.Location = new System.Drawing.Point(163, 59);
            this.rebootCb.Name = "rebootCb";
            this.rebootCb.Size = new System.Drawing.Size(90, 17);
            this.rebootCb.TabIndex = 7;
            this.rebootCb.Text = "Redémarrage";
            this.rebootCb.UseVisualStyleBackColor = true;
            // 
            // cancelShutdown
            // 
            this.cancelShutdown.Location = new System.Drawing.Point(150, 115);
            this.cancelShutdown.Name = "cancelShutdown";
            this.cancelShutdown.Size = new System.Drawing.Size(117, 23);
            this.cancelShutdown.TabIndex = 9;
            this.cancelShutdown.Text = "Annuler les arrêts";
            this.cancelShutdown.UseVisualStyleBackColor = true;
            this.cancelShutdown.Click += new System.EventHandler(this.cancel_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(163, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Jours";
            // 
            // dayNumber
            // 
            this.dayNumber.Location = new System.Drawing.Point(113, 21);
            this.dayNumber.Name = "dayNumber";
            this.dayNumber.Size = new System.Drawing.Size(44, 20);
            this.dayNumber.TabIndex = 10;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(415, 150);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.dayNumber);
            this.Controls.Add(this.cancelShutdown);
            this.Controls.Add(this.rebootCb);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.minuteNumber);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.hourNumber);
            this.Controls.Add(this.launchShutdown);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Programmer un arrêt planifié";
            ((System.ComponentModel.ISupportInitialize)(this.hourNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minuteNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dayNumber)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button launchShutdown;
        private System.Windows.Forms.NumericUpDown hourNumber;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown minuteNumber;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox rebootCb;
        private System.Windows.Forms.Button cancelShutdown;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown dayNumber;
    }
}

