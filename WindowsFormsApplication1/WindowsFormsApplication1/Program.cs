﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    static class Program
    {
        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainWindow());

            
            //Permet d'executer un programme externe
            //var process = new Process();
            //process.StartInfo = new ProcessStartInfo("ping", args[0])
            //{
                //Permet d'executer le nouveau process dans la même fenêtre de console
                //UseShellExecute = false


            //};
            //process.Start();
            //process.WaitForExit();
            //var exitCode = process.ExitCode;
            /*Console.WriteLine(exitCode);
            Console.ReadLine();*/
        }
    }
}
