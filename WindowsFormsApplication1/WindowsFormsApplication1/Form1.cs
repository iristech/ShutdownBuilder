﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class MainWindow : Form
    {

        public int time = 0;

        public MainWindow()
        {
            InitializeComponent();
        }


        private void launch_Click(object sender, EventArgs e)
        {
            time = 0;

            //faire un shutdown -a puis lancer le shutdown -r /f /t xxxxx
            if (hourNumber.Value != 0 || minuteNumber.Value != 0 || dayNumber.Value != 0)
            {
                checkIfNumericsAreEmpty();

                abortShutdown();

                time = Decimal.ToInt32( (dayNumber.Value * 24 ) * 3600 + hourNumber.Value * 3600 + minuteNumber.Value * 60);

                string args = "";

                if (rebootCb.Checked)
                {
                    args = args + "-r -f -t "+ time.ToString();
                }
                else
                {
                    args = args + "-s -f -t " + time.ToString();
                }

                shutdown(args);

            }
            else
            {
                MessageBox.Show("Veuillez rentrer un délai pour l'arrêt planifié !");
            }

        }


        private void cancel_Click(object sender, EventArgs e)
        {
            checkIfNumericsAreEmpty();
            abortShutdown();
        }

        private void abortShutdown()
        {
            var processAbort = new Process();
            processAbort.StartInfo = new ProcessStartInfo("shutdown", "-a")
            {

                //Permet d'executer le nouveau process dans la même fenêtre de console
                UseShellExecute = false,
                CreateNoWindow = true

            };
            processAbort.Start();
            processAbort.WaitForExit();
        }

        private void shutdown(string args)
        {
            var process = new Process();
            process.StartInfo = new ProcessStartInfo("shutdown", args)
            {

                //Permet d'executer le nouveau process dans la même fenêtre de console
                UseShellExecute = false,
                CreateNoWindow = true

            };
            process.Start();
            process.WaitForExit();
        }

        private void checkIfNumericsAreEmpty()
        {
            if (hourNumber.Text == "")
            {
                hourNumber.Text = "0";
                hourNumber.Value = 0;
            }
            if (minuteNumber.Text == "")
            {
                minuteNumber.Text = "0";
                minuteNumber.Value = 0;
            }
            if(dayNumber.Text == "")
            {
                dayNumber.Text = "0";
                dayNumber.Value = 0;
            }
        }
    }
}
